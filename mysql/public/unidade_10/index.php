<?php 

    require_once("../../conexao/conexao.php"); 
    require_once ("./vendor/autoload.php");
  
    $loader = new \Twig\Loader\FilesystemLoader('view');
    $twig = new \Twig\Environment($loader, [
        'cache' => 'view/cache/',
        'cache' => false
    ]);

    $template = $twig->Load('teste.twig');
    // $template->display();
    echo $template->render(array('valor1'=>'Estou aqui mais uma vez estudando'));
