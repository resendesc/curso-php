/ **
 * cbpScroller.js v1.0.0
 * http://www.codrops.com
 *
 * Licenciado sob a licença do MIT.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2013, Codrops
 * http://www.codrops.com
 * / 
; ( função ( janela ) {
 	
	'use strict' ;

	var docElem = window .document.documentElement;

	função  getViewportH () {
		 var client = docElem [ 'clientHeight' ],
			inner = window [ 'innerHeight' ];
		
		if (client <inner)
			 retorna interno;
		outro
			 cliente de retorno ;
	}

	função  scrollY () {
		 janela de retorno  .pageYOffset || docElem.scrollTop;
	}

	// http://stackoverflow.com/a/5598797/989439 
	função  getOffset ( el ) {
		 var offsetTop = 0 , offsetLeft = 0 ;
		do {
			 if (! isNaN (el.offsetTop)) {
				offsetTop + = el.offsetTop;
			}
			if (! isNaN (el.offsetLeft)) {
				offsetLeft + = el.offsetLeft;
			}
		} while (el = el.offsetParent)

		return {
			 top : offsetTop,
			 left : offsetLeft
		}
	}

	função  inViewport ( el, h ) {
		 var elH = el.offsetHeight,
			rolado = scrollY (),
			visto = rolado + getViewportH (),
			elTop = getOffset (el) .top,
			elBottom = elTop + elH,
			// se 0, o elemento é considerado na viewport assim que entra. 
			// se 1, o elemento é considerado na viewport somente quando estiver totalmente dentro 
			// valor em porcentagem (1> = h> = 0) 
			h = h || 0 ;

		return (elTop + elH * h) <= visualizado && (elBottom)> = rolado;
	}

	function  extend ( a, b ) {
		 para ( var key in b) { 
			 se (b.hasOwnProperty (key)) {
				a [tecla] = b [tecla];
			}
		}
		return a;
	}

	função  cbpScroller ( el, opções ) {	
		 this .el = el;
		this .options = extend ( isto .defaults, opções);
		isso ._init ();
	}

	cbpScroller.prototype = {
		padrões : {
			 // O viewportFactor define quanto do item que aparece deve ser visível para acionar a animação 
			// se usarmos um valor de 0, isso significaria adicionar a classe de animação assim que item está na viewport. 
			// Se usássemos o valor de 1, a animação só seria acionada quando víssemos todo o item na viewport (100% dele) 
			viewportFactor: 0.2
		}
		_init : function () {
			 if (Modernizr.touch) retornar ;
			this .sections = Array .prototype.slice.call ( este .el.querySelectorAll ( '.cbp-so-section' ));
			isso .didScroll = false ;

			var self = this ;
			// as seções já mostradas ... 
			this .sections.forEach ( function ( el, i ) {
				 if (! inViewport (el)) {
					classie.add (el, 'cbp-so-init' );
				}
			});

			var scrollHandler = função () {
					 if (! self.didScroll) {
						self.didScroll = true ;
						setTimeout ( função () {self._scrollPage (); }, 60 );
					}
				}
				resizeHandler = function () {
					 função  atrasada () {
						self._scrollPage ();
						self.resizeTimeout = null ;
					}
					if (self.resizeTimeout) {
						clearTimeout (self.resizeTimeout);
					}
					self.resizeTimeout = setTimeout (atrasado, 200 );
				};

			janela .addEventListener ( 'scroll' , scrollHandler, false );
			window .addEventListener ( 'resize' , resizeHandler, false );
		}
		_scrollPage : function () {
			 var self = isto ;

			this .sections.forEach ( função ( el, i ) {
				 if (inViewport (el, self.options.viewportFactor)) {
					classie.add (el, 'cbp-so-animate' );
				}
				else {
					 // isso adiciona a classe init se ela não tiver. Isso garantirá que os itens inicialmente na viewport também sejam animados em scroll 
					classie.add (el, 'cbp-so-init' );
					
					classie.remove (el, 'cbp-so-animate' );
				}
			});
			isso .didScroll = false ;
		}
	}

	// adiciona à 
	janela de namespace global .cbpScroller = cbpScroller;

}) ( janela );