<?php require_once("../../conexao/conexao.php"); ?>


<?php
		//passo 3 abrir consulta ao banco de dados
		$consulta_categorias = "SELECT nomeproduto, tempoentrega";
		$consulta_categorias .= " FROM produtos";
		// $consulta_categorias .= " WHERE categoriaID > 2";
		$categorias = mysqli_query($conecta, $consulta_categorias);

		if (!$categorias){
			die("Falha na consulta ao banco.");
		}



?>

<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Curso PHP FUNDAMENTAL</title>
    </head>

    <body>
    	
    	<ul>
	    	<?php
	    	//passo 4 listagem dos dados
	    		while ($registro = mysqli_fetch_assoc($categorias)) {
	    	?>

	    		<li><?php echo $registro["nomeproduto"]." - tempo de entrega ".$registro["tempoentrega"] ?></li>		
	    		

	    	<?php
	    		}
	    	?>
    	</ul>


    	<?php
    	// passo 5 - liberar dados da memoria
    		mysqli_free_result($categorias);
    	?>

    </body>
</html>


<?php
// passo 6 - fechar conexão ao banco de dados
	mysqli_close($conecta);
?>