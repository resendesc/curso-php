<?php require_once("../../conexao/conexao.php"); ?>
<?php require_once("teste_seguranca.php"); ?>

<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Curso PHP FUNDAMENTAL</title>
        
        <!-- estilo -->
        <link href="_css/estilo.css" rel="stylesheet">
    </head>

    <body>
        <?php include_once("_incluir/topo.php"); ?>
        
        
        <main>  
            <?php
                echo "Seja bem vindo ".$_SESSION["usuario"];
            ?>
            <p>
                <a href="detalhe.php">Detalhes</a><br />
                <a href="listagem.php">Listagem</a><br />
                <a href="pagina1.php">Página 1</a><br />
                <a href="pagina2.php">Página 2</a><br />
                <a href="logout.php">Sair</a>
            </p>
            
        </main>

        <footer>
            <div id="footer_central">
                <p>ANDES &eacute; uma empresa fict&iacute;cia, usada para o curso PHP Integra&ccedil;&atilde;o com MySQL.</p>
            </div>
        </footer>
    </body>
</html>

<?php
    // Fechar conexao
    mysqli_close($conecta);
?>