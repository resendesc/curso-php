<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Curso PHP FUNDAMENTAL</title>
    </head>

    <body>
    	<?php

    		//determinar timezone
    		date_default_timezone_set('Americana/Sao_Paulo');
    		
            // setlocale (LC_TIME, "pt_BR");

            setlocale(LC_ALL, 'pt_BR.utf-8');


            // $agora = getdate();
    		
    		//criar elementos

    		$ano = strftime('%Y');
    		$mes = strftime('%B');
    		$dia = strftime('%d');

            $dia_semana = strftime('%A');

    		$hora     = strftime('%H');
    		$minuto   = strftime('%M');
    		$segundo  = strftime('%S');

    		// if ($minuto <= 9) {
    		// 	$minuto = 0 . $minuto;
    		// }

    		// if ($hora <= 9) {
    		// 	$hora = 0 . $hora;
    		// }

    		// if ($segundo <= 9) {
    		// 	$segundo = 0 . $segundo;
    		// }

    		echo $dia_semana . "-feira, " . $dia . " de " . $mes . " de " . $ano . " - " . $hora . ":" . $minuto . ":" . $segundo;


    	?>
    </body>
</html>