<?php require_once("../../conexao/conexao.php"); ?>
<?php require_once("teste_seguranca.php"); ?>

<?php
    // iniciar a sessão
    session_start();
    // criar uma variável de sessão
?>
<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Curso PHP FUNDAMENTAL</title>
        
        <!-- estilo -->
        <link href="_css/estilo.css" rel="stylesheet">
    </head>

    <body>
        <header>
            <div id="header_central">
                <img src="assets/logo_andes.gif">
                <img src="assets/text_bnwcoffee.gif">
            </div>
        </header>
        
        <main>
            <?php
                // excluir a variavel sessão
                // unset($_SESSION["usuario"]);
                // destroi todas as variaveis
                session_destroy();
                header("location:login.php");

            ?>  
            
        </main>

        <footer>
            <div id="footer_central">
                <p>ANDES &eacute; uma empresa fict&iacute;cia, usada para o curso PHP Integra&ccedil;&atilde;o com MySQL.</p>
            </div>
        </footer>
    </body>
</html>

<?php
    // Fechar conexao
    mysqli_close($conecta);
?>